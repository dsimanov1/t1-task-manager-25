package ru.t1.simanov.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.simanov.tm.comparator.CreatedComparator;
import ru.t1.simanov.tm.comparator.NameComparator;
import ru.t1.simanov.tm.comparator.StatusComparator;

import java.util.Comparator;

@Getter
public enum Sort {

    BY_NAME("Sort by name", NameComparator.INSTANCE),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE);

    @NotNull
    private final String displayName;

    @NotNull
    private final Comparator comparator;

    Sort(@NotNull final String displayName, @NotNull final Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    @Nullable
    public static Sort toSort(@NotNull final String value) {
        if (value.isEmpty()) return null;
        for (@Nullable final Sort sort : values()) {
            assert sort != null;
            if (sort.name().equals(value)) return sort;
        }
        return null;
    }

}

